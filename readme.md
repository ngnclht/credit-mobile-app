## Setup

To set up the application, make sure you have the standard react-native environment, please follow [https://reactnative.dev/docs/environment-setup](https://reactnative.dev/docs/environment-setup)

Then navigate to the root folder and install dependencies:

```jsx
yarn install
cd ios && pod install
```

To run the ios app, open the project via Xcode and pick the staging scheme, and just hit the run button:

![https://cln.sh/obNvhG](https://cln.sh/obNvhG/download)

To run the android app, navigate to the root folder:

```jsx
yarn android debitApp staging

```

#### How to run unit test

Just navigate to the root folder and hit:

```jsx
yarn test
```

## Technologies summary

The app is using:

- Typescript for better typing.
- Redux for dataflow management.
- Redux Toolkit for faster coding speed.
- Redux Persist in saving the data to local storage (with standard encryption for better security).
- React Native Config to separate the app environment.
- Reactoron as debugger tool.
- Redux saga for handling logic.
- I18next for handling multiple languages
- React Native Codepush and Semver for handling OTA updates.
- React Native Responsive Screen for responsive sizes.
- React Navigation for screen management.
- CryptoJS for async storage encryption.
- Husky: automatically formats the code using Prettier, so we have a good format.

## Application summary

- We have 2 environments, staging and production for both android && ios.
- We have supported the dark && light theme (Color is not clearly defined, but it is ready).
- We have auto-update via code push
- We have variable's type definition without the need of importing.
- We have multiple languages ready
- We have added firebase config for both IOS && android, both staging, and production as well, in case we need to support multiple environment's firebase configs. (Firebase ready)
- We have Gitlab CI to run test.

## Principle

### Module

The app is separated into multiple modules, each module takes responsibility for a specific domain.
A module must contain all assets, saga, reducers, screens inside its folder.
A module can use global's resources like UI components that are shared by many modules.
A module can contain other smaller modules

### Component

A single component if shared by many modules should be placed in the global UI folder. (`UI/component`)

All the assets needed for the component and not shared with others, need to be placed inside its own assets folder.

## The architecture:

Here is an overview of the folder structure in the project:

![download](https://cln.sh/T43GAl/download)

I have drawed here for more detail:
![download](https://cln.sh/sjz04z/download)

I have divided the project into multiple modules, each module contains all the specific stuff of the module. In this project, we have two modules:

- The app module: handle the application's configuration, handle the authentication flow, handle codepush, etc. The app module has 3 screens: Splash screen, Onboarding screen, and Log-in screen.
  The app module also has a `registry` folder which takes responsibility for registering other's module reducer, saga, screen, etc.
  The app module is the main module of the application.
  All assets like reducer, saga, type... are separately inside the module's folder.
- The Dashboard module: Manage the tabs, each tab is a single module.
  All assets like reducer, saga, type... are separately inside the module's folder.
  The Debit module is a child of the Dashboard module, it takes responsibility for all features of the view & update debit card.

I also made a short video to introduce the source code summary:
[Project summary](https://drive.google.com/file/d/104mZc4iaLCxpfRhN8t78TkNPuyyB6B0H/view?usp=sharing)

This is the youtube version, but sorry that it's too blur!
[Project Summary Youtube (it's a bit blur)](https://youtu.be/c-09riaZW9E)

## Application flow:

This is the authentication flow

![Screen Shot 2021](https://cln.sh/F2Gw4e/download)

Sagas:

- init.saga.ts: this will check the authentication state
- auth.saga.ts: this will handle login logic

## How I did it:

### Deal with schema

I have been looking around the UI design, and define a schema. The schema is just a simple schema

```
type DebitCard = {
    createdAt: string
    accountName: string
    accountNumber: string
    ccv: string
    date: string
    id: string
    hasWeeklySpendingLimit: boolean
    weeklySpendingLimit: number
    currencyCode: string
    currencySymbol: string
    balance: number
    currentSpendWithinWeek: number
}
```

When we have turn on the limit, we also change the hasWeeklySpendingLimi's flag as well.
Thanks to mockapi.io. I used their service to handle the BE's part.

### Deal with UI

I took a look in the UI and try to break things down in to multiple components that we can reuse. The components I have defined:

- AppHeader: use by globaly in many screens.
- AppFontText: of course, it used globaly.
- Card: component used only by DebitCard module. So I put it inside DebitCard's folder.
- ActionMenuItem: used only by DebitCard module. So I put it inside DebitCard's folder.
- etc.

### Define redux's store

I used redux to define the data for the app.
I defined a reducer for storing the debit card data. The reducer's shape looks like this.

```
type DebitCardReducer = {
    debitCard?: DebitCard
    fetchingDebitCard?: boolean
    fetchDebitCardError?: string
    settingWeeklySpendingLimit?: boolean
    setWeeklySpendingLimitError?: string
}
```

## APK File:

To make it easier for the reviewer, I have build the android app here. Please download the android build APK from this link.
(The old link is broken so I updated the new link in newest commit)
[Android APK](https://drive.google.com/file/d/1uGRiEqySLXNNrLVAfpYBRQJcKYw8vHZQ/view?usp=sharing)

## Demo video:

[Android Demo](https://drive.google.com/file/d/1tIzO5KLtx8coxbg-fMiZJWbZMT2Z5Iao/view?usp=sharing)

[IOS Demo](https://drive.google.com/file/d/1rTXRnQtrHOnzRMyXjwT0IO1f0mBklt1N/view?usp=sharing)

I am so sorry that the youtube's videos are too blur

The youtube version is here:

[Android Demo](https://www.youtube.com/watch?v=NLDnC2biI5Y)

[IOS Demo](https://youtu.be/ArY1vGoiWXI)
