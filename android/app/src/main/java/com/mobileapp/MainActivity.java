package com.mobileapp;
import android.os.Bundle; // react navigation v6

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {
  // required by react navigation v6
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(null);
  }
  // end required by react navigation v6

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "MobileApp";
  }
}
