/**
 * @format
 */
import 'react-native-gesture-handler'
import '@m_app/registry/i18n'
import { AppRegistry } from 'react-native'
import AppWithCodepush from './src/modules/app/app-with-codepush'
import { name as appName } from './app.json'

AppRegistry.registerComponent(appName, () => AppWithCodepush)
