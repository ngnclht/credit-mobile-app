module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        alias: {
          '@themes': './src/ui/themes',
          '@sizes': './src/ui/sizes',
          '@utils': './src/utils',
          '@m_app': './src/modules/app',
          '@m_dashboard': './src/modules/dashboard',
          '@components': './src/ui/components',
          '@m_dashboard_home': './src/modules/dashboard/modules/home',
          '@m_dashboard_credit': './src/modules/dashboard/modules/credit',
          '@m_dashboard_profile': './src/modules/dashboard/modules/profile',
          '@m_dashboard_payment': './src/modules/dashboard/modules/payment',
          '@m_dashboard_debit_card':
            './src/modules/dashboard/modules/debit-card',
          '@': './src'
        }
      }
    ]
  ]
}
