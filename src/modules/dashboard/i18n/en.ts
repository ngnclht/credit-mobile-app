import { debitCardModuleLang } from '@m_dashboard_debit_card/i18n'

const en = {
  title: 'Dashboard',
  tab_home: 'Home',
  tab_profile: 'Profile',
  tab_credit: 'Credit',
  tab_debit_card: 'Debit card',
  tab_payment: 'Payment',
  ...debitCardModuleLang.en
}

export default en
