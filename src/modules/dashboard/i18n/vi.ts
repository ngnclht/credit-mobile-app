import { debitCardModuleLang } from '@m_dashboard_debit_card/i18n'

const vi = {
  title: 'Hệ thống',
  tab_home: 'Trang chủ',
  tab_profile: 'Tài khoản',
  ...debitCardModuleLang.vi
}

export default vi
