import React from 'react'
import { SafeAreaView } from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'
import styles from './styles'

const PaymentTab = (props: any) => {
  const { t } = useTranslation()
  return (
    <SafeAreaView style={styles.wrapper}>
      <FontText style={styles.title}>{t('dashboard.title')}</FontText>
    </SafeAreaView>
  )
}

export default PaymentTab
