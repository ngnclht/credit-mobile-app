import { put } from 'redux-saga/effects'
import { debitCardActions } from '@m_dashboard_debit_card/reducers/debit-card.reducer'

export function* debitCardPostLogin() {
  yield put(debitCardActions.actDebitCard_fetchDebitCardInfo())
}
