import { PayloadAction } from '@reduxjs/toolkit'
import { call, put, takeLeading, select } from 'redux-saga/effects'
import { Alert } from 'react-native'
import {
  debitCardActions,
  debitCardSelectors
} from '@m_dashboard_debit_card/reducers/debit-card.reducer'
import { apiSetWeeklySendingLimit } from '@m_dashboard_debit_card/api'
import { i18n } from '@m_app/registry/i18n'
import { navService_GoBack } from '@m_app/utils/navigation/navigation-services'

function* setWeelyLimitForDebitCardFlow(action: PayloadAction<number>) {
  try {
    const debitCard = yield select(debitCardSelectors.getDebitCard)
    const res = yield call(apiSetWeeklySendingLimit, {
      debitCard,
      limit: action.payload
    })
    yield put(debitCardActions.actDebitCard_setWeeklyLimit_Success(res))
    Alert.alert(
      i18n.t('dashboard.debitCard_set_limit_success_title'),
      i18n.t('dashboard.debitCard_set_limit_success_content')
    )
    yield call(navService_GoBack)
  } catch (error) {
    // TODO show error message in the UI (for a nice UI)
    Alert.alert(error.message)
    // TODO Capture this error message and send to sentry
    yield put(
      debitCardActions.actDebitCard_setWeeklyLimit_Failed(error.message)
    )
  }
}

export function* setWeelyLimitForDebitCardWatcher() {
  yield takeLeading(
    debitCardActions.actDebitCard_setWeeklyLimit.type,
    setWeelyLimitForDebitCardFlow
  )
}
