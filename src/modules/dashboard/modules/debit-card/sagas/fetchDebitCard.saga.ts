import { PayloadAction } from '@reduxjs/toolkit'
import { call, put, takeLeading } from 'redux-saga/effects'
import { Alert } from 'react-native'
import { debitCardActions } from '@m_dashboard_debit_card/reducers/debit-card.reducer'
import { apiFetchDebitCard } from '@m_dashboard_debit_card/api'

function* fetchDebitCardFlow(_action: PayloadAction<undefined>) {
  try {
    const res = yield call(apiFetchDebitCard)
    yield put(debitCardActions.actDebitCard_fetchDebitCardInfo_Success(res))
  } catch (error) {
    // TODO show error message in the UI (for a nice UI)
    Alert.alert(error.message)
    // TODO Capture this error message and send to sentry
    yield put(
      debitCardActions.actDebitCard_fetchDebitCardInfo_Failed(error.message)
    )
  }
}

export function* fetchDebitCardWatcher() {
  yield takeLeading(
    debitCardActions.actDebitCard_fetchDebitCardInfo.type,
    fetchDebitCardFlow
  )
}
