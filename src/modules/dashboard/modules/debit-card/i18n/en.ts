const en = {
  debitCard_hide_card_number: 'Hide card number',
  debitCard_show_card_number: 'Show card number',
  debitCard_available_balance: 'Available balance',
  debitCard_exp_date: 'Thru',
  debitCard_ccv: 'CCV',
  debitCard_debit_card_spend_limit: 'Debit card spending limit',
  debitCard_menu_topup_account: 'Top-up account',
  debitCard_menu_topup_account_subtitle:
    'Deposit money to your account to use with card',
  debitCard_menu_weekly_spending_limit: 'Weekly spending limit',
  debitCard_menu_weekly_spending_limit_subtitle:
    'Your weekly spending limit is',
  debitCard_menu_weekly_spending_limit_subtitle_no_limit:
    'You haven’t set any spending limit on card',
  debitCard_menu_freeze_card: 'Freeze card',
  debitCard_menu_freeze_card_subtitle: 'Your debit card is currently active',
  debitCard_menu_get_new_card: 'Get a new card',
  debitCard_menu_get_new_card_subtitle:
    'This deactivates your current debit card',
  debitCard_menu_deactivatated_card: 'Deactivated cards',
  debitCard_menu_deactivatated_card_subtitle:
    'Your previously deactivated cards',
  debitCard_spending_limit: 'Spending limit',
  debitCard_spending_limit_title: 'Set a weekly debit card spending limit',
  debitCard_weekly_explained:
    'Here weekly means the last 7 days - not the calendar week',
  debitCard_feature_missing: 'Feature is not implemented yet',
  debitCard_save: 'Save',
  debitCard_set_limit_success_content: 'Set weekly limit successfully!',
  debitCard_set_limit_success_title: 'Cool, you made it!'
}

export default en
