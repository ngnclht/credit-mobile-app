import { ReduxStore } from '@m_app/configs/store.config'
import { debitCardActions } from '@m_dashboard_debit_card/reducers/debit-card.reducer'

const getState = (): DEBITCARD.DebitCardReducer => {
  let appState: APP.RootState = ReduxStore.getState()
  const state = appState.debitCard
  return state
}

const mockDebitCard = {
  createdAt: '2020/11/11',
  accountName: 'Curtis John',
  accountNumber: '4450345264553665',
  ccv: '779',
  date: '08/19',
  hasWeeklySpendingLimit: false,
  weeklySpendingLimit: 0,
  currencyCode: 'SGD',
  currencySymbol: 'S$',
  balance: 100000,
  currentSpendWithinWeek: 500,
  id: '3'
}

test('Initial state check', () => {
  let state = getState()
  expect(state.fetchingDebitCard).toBe(false)
  expect(state.fetchDebitCardError).toBe(undefined)
  expect(state.debitCard).toBe(undefined)
})

test('test for state after fetchDebitCardInfo dispatched', () => {
  ReduxStore.dispatch(debitCardActions.actDebitCard_fetchDebitCardInfo())
  let state = getState()
  expect(state.fetchingDebitCard).toBe(true)
  expect(state.fetchDebitCardError).toBe('')
})

test('test for state after actDebitCard_fetchDebitCardInfo_Success dispatched', () => {
  ReduxStore.dispatch(debitCardActions.actDebitCard_fetchDebitCardInfo())
  ReduxStore.dispatch(
    debitCardActions.actDebitCard_fetchDebitCardInfo_Success(mockDebitCard)
  )
  let state = getState()
  expect(state.fetchingDebitCard).toBe(false)
  expect(state.fetchDebitCardError).toBe('')
  expect(state.debitCard).toBe(mockDebitCard)
})

test('test for state after actDebitCard_fetchDebitCardInfo_Failed dispatched', () => {
  ReduxStore.dispatch(debitCardActions.actDebitCard_fetchDebitCardInfo())
  ReduxStore.dispatch(
    debitCardActions.actDebitCard_fetchDebitCardInfo_Failed('Un-expected error')
  )
  let state = getState()
  expect(state.fetchingDebitCard).toBe(false)
  expect(state.fetchDebitCardError).toBe('Un-expected error')
})

test('test for state after setWeeklyLimit dispatched', () => {
  ReduxStore.dispatch(
    debitCardActions.actDebitCard_fetchDebitCardInfo_Success(mockDebitCard)
  )
  ReduxStore.dispatch(debitCardActions.actDebitCard_setWeeklyLimit(5000))
  let state = getState()
  expect(state.settingWeeklySpendingLimit).toBe(true)
  expect(state.setWeeklySpendingLimitError).toBe('')
})

test('test for state after actDebitCard_setWeeklyLimit_Success dispatched', () => {
  ReduxStore.dispatch(
    debitCardActions.actDebitCard_setWeeklyLimit_Success({
      ...mockDebitCard,
      weeklySpendingLimit: 5000,
      hasWeeklySpendingLimit: true
    })
  )
  let state = getState()
  expect(state.settingWeeklySpendingLimit).toBe(false)
  expect(state.setWeeklySpendingLimitError).toBe('')
  expect(state.debitCard?.hasWeeklySpendingLimit).toBe(true)
  expect(state.debitCard?.weeklySpendingLimit).toBe(5000)
})

test('test for state after actDebitCard_setWeeklyLimit_Failed dispatched', () => {
  ReduxStore.dispatch(
    debitCardActions.actDebitCard_setWeeklyLimit_Failed('Un-expected error')
  )
  let state = getState()
  expect(state.settingWeeklySpendingLimit).toBe(false)
  expect(state.setWeeklySpendingLimitError).toBe('Un-expected error')
})
