import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState: DEBITCARD.DebitCardReducer = {
  debitCard: undefined,
  fetchDebitCardError: undefined,
  fetchingDebitCard: false,
  settingWeeklySpendingLimit: false,
  setWeeklySpendingLimitError: undefined
}

export const debitCardReducer = createSlice({
  name: 'debitCard',
  initialState,
  reducers: {
    actDebitCard_fetchDebitCardInfo(state, _action: PayloadAction<undefined>) {
      state.fetchDebitCardError = ''
      state.fetchingDebitCard = true
    },
    actDebitCard_fetchDebitCardInfo_Success(
      state,
      action: PayloadAction<DEBITCARD.DebitCard>
    ) {
      state.fetchDebitCardError = ''
      state.fetchingDebitCard = false
      state.debitCard = action.payload
    },
    actDebitCard_fetchDebitCardInfo_Failed(
      state,
      action: PayloadAction<string>
    ) {
      state.fetchDebitCardError = action.payload
      state.fetchingDebitCard = false
    },
    actDebitCard_setWeeklyLimit(state, _action: PayloadAction<number>) {
      state.setWeeklySpendingLimitError = ''
      state.settingWeeklySpendingLimit = true
    },
    actDebitCard_setWeeklyLimit_Success(
      state,
      action: PayloadAction<DEBITCARD.DebitCard>
    ) {
      state.setWeeklySpendingLimitError = ''
      state.settingWeeklySpendingLimit = false
      state.debitCard = action.payload
    },
    actDebitCard_setWeeklyLimit_Failed(state, action: PayloadAction<string>) {
      state.setWeeklySpendingLimitError = action.payload
      state.settingWeeklySpendingLimit = false
    }
  }
})

export const debitCardActions = debitCardReducer.actions

export const debitCardSelectors = {
  getDebitCard: (state: APP.RootState) => state.debitCard.debitCard,
  fetchDebitCardError: (state: APP.RootState) =>
    state.debitCard.fetchDebitCardError,
  fetchingDebitCard: (state: APP.RootState) =>
    state.debitCard.fetchingDebitCard,
  setWeeklySpendingLimitError: (state: APP.RootState) =>
    state.debitCard.setWeeklySpendingLimitError,
  settingWeeklySpendingLimit: (state: APP.RootState) =>
    state.debitCard.settingWeeklySpendingLimit
}
