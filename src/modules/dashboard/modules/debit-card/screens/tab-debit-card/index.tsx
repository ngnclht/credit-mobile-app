import React, { useState } from "react";
import { SafeAreaView, ScrollView, StatusBar, View } from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'
import VisaCardComponent from '@m_dashboard_debit_card/components/visa-card'
import DebitActionMenuItem from '@m_dashboard_debit_card/components/menu-item'
import SpendingLimitBar from '@m_dashboard_debit_card/components/spending-limit-bar'
import AppHeader from '@components/header'
import {
  DEBITCARD_ACTION_MENU,
  WEEKLY_SPENDING_MENU_ID
} from '@m_dashboard_debit_card/constants'
import styles from './styles'
import THEMES from '@themes/theme'
import CurrencyDecorator from '@m_dashboard_debit_card/components/currency-decorator'
import { useSelector } from 'react-redux'
import { debitCardSelectors } from '@m_dashboard_debit_card/reducers/debit-card.reducer'
import { appUtil_formatMoney } from '@utils/currency'

const DebitCardTab = () => {
  const { t } = useTranslation()
  const [hideSensitive, setHideSensitive] = useState(true)
  const debitCard = useSelector(debitCardSelectors.getDebitCard)
  const fetchDebitCardError = useSelector(
    debitCardSelectors.fetchDebitCardError
  )
  const fetchingDebitCard = useSelector(debitCardSelectors.fetchingDebitCard)

  return (
    <SafeAreaView style={styles.wrapper}>
      <StatusBar backgroundColor={THEMES.BACKGROUND_2} />
      <AppHeader title={t('dashboard.tab_debit_card')} showBackButton={false} />
      <FontText style={styles.subtitle}>
        {t('dashboard.debitCard_available_balance')}
      </FontText>

      <View style={styles.amountContainer}>
        <CurrencyDecorator
          currencyAlias={debitCard?.currencySymbol || '$'}
          value={
            debitCard?.balance ? appUtil_formatMoney(debitCard?.balance) : '--'
          }
          textColor={THEMES.TEXT_1}
        />
      </View>

      <View style={styles.container}>
        <ScrollView bounces={false}>
          <View style={styles.emptyView} />
          <View style={styles.contentView}>
            <VisaCardComponent
              fullName={debitCard?.accountName || ''}
              accountNumber={debitCard?.accountNumber || ''}
              expDate={debitCard?.date || ''}
              ccv={debitCard?.ccv || ''}
              brand={'visa'}
              hideSensive={hideSensitive}
              setHideSensive={setHideSensitive}
            />
            <View style={styles.mainBackground}>
              {debitCard?.hasWeeklySpendingLimit ? (
                <SpendingLimitBar
                  total={debitCard?.weeklySpendingLimit || '--'}
                  limit={debitCard?.currentSpendWithinWeek || '--'}
                  currencySymbol={debitCard?.currencySymbol}
                />
              ) : null}
              {DEBITCARD_ACTION_MENU.map((menu) => {
                const isWeeklySpendingMenu = menu.id == WEEKLY_SPENDING_MENU_ID
                const booleanStatusValue = isWeeklySpendingMenu
                  ? debitCard?.hasWeeklySpendingLimit
                  : false
                let subtitle = menu.subtitle
                if (isWeeklySpendingMenu) {
                  subtitle = debitCard?.hasWeeklySpendingLimit
                    ? `${menu.subtitle} ${appUtil_formatMoney(
                        debitCard?.weeklySpendingLimit,
                        false,
                        debitCard?.currencySymbol
                      )}`
                    : menu.subtitleWhenBooleanValueIsOff || ''
                }
                return (
                  <DebitActionMenuItem
                    key={menu.title}
                    title={menu.title}
                    subtitle={subtitle}
                    icon={menu.icon}
                    hasBooleanStatus={menu.hasBooleanStatus}
                    booleanStatusValue={booleanStatusValue}
                    onTouch={menu.onTouch}
                  />
                )
              })}
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  )
}

export default DebitCardTab
