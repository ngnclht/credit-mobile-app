import { hp2dp, SIZES, wp2dp } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { BORDER_RADIUS } from '@m_dashboard_debit_card/components/visa-card/styles'
import { isIOS } from '@m_app/utils/info'

const TOP_VIEW_HEIGHT = isIOS ? hp2dp(21) : hp2dp(19)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: THEMES.BACKGROUND_2
  },
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },
  emptyView: {
    height: TOP_VIEW_HEIGHT
  },
  contentView: {
    backgroundColor: 'transparent'
  },
  amountContainer: {
    paddingHorizontal: SIZES.UI_PADDING_MARGIN
  },
  mainBackground: {
    paddingTop: isIOS ? TOP_VIEW_HEIGHT * 1 : TOP_VIEW_HEIGHT * 1.2,
    marginTop: isIOS ? -TOP_VIEW_HEIGHT * 0.9 : -TOP_VIEW_HEIGHT * 1.1,
    borderTopLeftRadius: BORDER_RADIUS * 2,
    borderTopRightRadius: BORDER_RADIUS * 2,
    backgroundColor: THEMES.BACKGROUND_1,
    zIndex: 100
  },
  title: {
    marginTop: hp2dp(2.5),
    fontSize: SIZES.UI_FONT_SIZE_6,
    fontWeight: 'bold',
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    marginBottom: hp2dp(2.9),
    color: THEMES.TEXT_1
  },
  subtitle: {
    marginTop: hp2dp(2.9),
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    fontSize: SIZES.UI_FONT_SIZE_4,
    color: THEMES.TEXT_1,
    marginBottom: hp2dp(1.2)
  }
})

export default styles
