import React, { useState } from 'react'
import {
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  View
} from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'
import AppHeader from '@components/header'
import styles from './styles'
import THEMES from '@themes/theme'
import { debitCardModuleAssets } from '@m_dashboard_debit_card/assets'
import CurrencyDecorator from '@m_dashboard_debit_card/components/currency-decorator'
import AspireButton from '@components/app-button'
import { appDispatchAction } from '@m_app/utils/redux/dispatch-action'
import {
  debitCardActions,
  debitCardSelectors
} from '@m_dashboard_debit_card/reducers/debit-card.reducer'
import { DEBITCARD_LIMIT_LEVEL } from '@m_dashboard_debit_card/constants'
import { useSelector } from 'react-redux'
import { appUtil_formatMoney } from '@utils/currency'

const SetWeeklySpendingLimitScreen = (_props: any) => {
  const { t } = useTranslation()
  const [selectedLimit, setSelectedLimit] = useState<number>(0)
  const debitCard = useSelector(debitCardSelectors.getDebitCard)
  const settingWeeklySpendingLimit = useSelector(
    debitCardSelectors.settingWeeklySpendingLimit
  )
  const onSetLimit = appDispatchAction(
    debitCardActions.actDebitCard_setWeeklyLimit(selectedLimit)
  )

  return (
    <>
      <SafeAreaView style={styles.wrapper}>
        <StatusBar backgroundColor={THEMES.BACKGROUND_2} />
        <AppHeader
          title={t('dashboard.tab_debit_card')}
          showBackButton={true}
        />

        <View style={styles.mainBackground}>
          <View style={styles.container}>
            <View style={styles.titleContainer}>
              <Image
                source={debitCardModuleAssets.weeklyLimitIcon}
                style={styles.titleImage}
              />
              <FontText style={styles.title}>
                {t('dashboard.debitCard_spending_limit_title')}
              </FontText>
            </View>

            <View style={styles.resultView}>
              <CurrencyDecorator
                currencyAlias={debitCard?.currencySymbol || '--'}
                value={appUtil_formatMoney(
                  selectedLimit
                    ? selectedLimit
                    : debitCard?.hasWeeklySpendingLimit
                    ? debitCard?.weeklySpendingLimit
                    : 0,
                  true
                )}
                textColor={THEMES.TEXT_GRAY1}
                currencyDecoratorTitle={styles.currencyDecoratorAmount}
              />
            </View>

            <FontText style={styles.explained}>
              {t('dashboard.debitCard_weekly_explained')}
            </FontText>

            <View style={styles.selectionWrapper}>
              {DEBITCARD_LIMIT_LEVEL.map((level: number) => (
                <TouchableOpacity
                  key={`${level}`}
                  onPress={() => setSelectedLimit(level)}
                  style={[
                    styles.amountButton,
                    level === selectedLimit
                      ? styles.amountButtonSelected
                      : undefined
                  ]}
                >
                  <FontText style={styles.amountButtonText}>
                    {appUtil_formatMoney(
                      level,
                      true,
                      debitCard?.currencySymbol
                    )}
                  </FontText>
                </TouchableOpacity>
              ))}
            </View>
          </View>
          <AspireButton
            working={settingWeeklySpendingLimit}
            disabled={selectedLimit <= 0}
            title={t('dashboard.debitCard_save')}
            onTouch={onSetLimit}
          />
        </View>
      </SafeAreaView>
      <SafeAreaView style={styles.bottomSafeAreaView} />
    </>
  )
}

export default SetWeeklySpendingLimitScreen
