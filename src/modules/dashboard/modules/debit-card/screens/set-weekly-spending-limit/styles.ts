import { hp2dp, SIZES, wp2dp } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { BORDER_RADIUS } from '@m_dashboard_debit_card/components/visa-card/styles'
import { isIOS } from '@m_app/utils/info'

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: THEMES.BACKGROUND_2
  },
  mainBackground: {
    marginTop: hp2dp(6),
    borderTopLeftRadius: BORDER_RADIUS * 2,
    borderTopRightRadius: BORDER_RADIUS * 2,
    backgroundColor: THEMES.BACKGROUND_1,
    zIndex: 100,
    flex: 1,
    padding: SIZES.UI_PADDING_MARGIN,
    justifyContent: 'space-between'
  },
  container: {},
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleImage: {
    height: wp2dp(4.5),
    width: wp2dp(4.5),
    resizeMode: 'contain',
    marginRight: 5,
    marginBottom: 2.5
  },
  title: {
    fontSize: SIZES.UI_FONT_SIZE_4a,
    color: THEMES.TEXT_GRAY1
  },
  resultView: {
    marginTop: hp2dp(1.4),
    borderBottomColor: THEMES.BORDER_1,
    paddingBottom: 8,
    borderBottomWidth: 0.5
  },
  currencyDecoratorAmount: {
    marginTop: isIOS ? 5 : 0
  },
  explained: {
    fontSize: SIZES.UI_FONT_SIZE_3,
    color: THEMES.TEXT_GRAY1A,
    marginTop: hp2dp(1)
  },
  selectionWrapper: {
    marginTop: hp2dp(4),
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  amountButton: {
    borderRadius: 5,
    width: wp2dp(28),
    backgroundColor: THEMES.BACKGROUND_4,
    paddingVertical: wp2dp(4),
    alignItems: 'center',
    justifyContent: 'center'
  },
  amountButtonSelected: {
    backgroundColor: THEMES.BACKGROUND_2
  },
  amountButtonText: {
    color: THEMES.TEXT_2,
    fontSize: SIZES.UI_FONT_SIZE_2,
    fontWeight: '600'
  },
  bottomSafeAreaView: {
    backgroundColor: THEMES.BACKGROUND_1
  }
})

export default styles
