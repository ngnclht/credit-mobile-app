import { API_DEBITCARD } from './endpoint'
import { unauthenticatedRequest } from '@utils/api.helper'

const FIXED_ID = 3

export const apiFetchDebitCard = () => {
  const url = API_DEBITCARD + `/${FIXED_ID}`
  return unauthenticatedRequest('GET', url)
}

export const apiSetWeeklySendingLimit = (params: {
  debitCard: DEBITCARD.DebitCard
  limit: number
}) => {
  const url = API_DEBITCARD + `/${FIXED_ID}`
  const updated: DEBITCARD.DebitCard = {
    ...params.debitCard,
    weeklySpendingLimit: params.limit,
    hasWeeklySpendingLimit: params.limit ? true : false
  }
  return unauthenticatedRequest('PUT', url, updated)
}
