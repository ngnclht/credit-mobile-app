// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { ImageSourcePropType } from 'react-native'

export {}

declare global {
  namespace DEBITCARD {
    type DebitCardReducer = {
      debitCard?: DebitCard
      fetchingDebitCard?: boolean
      fetchDebitCardError?: string
      settingWeeklySpendingLimit?: boolean
      setWeeklySpendingLimitError?: string
    }

    type DebitCard = {
      createdAt: string
      accountName: string
      accountNumber: string
      ccv: string
      date: string
      id: string
      hasWeeklySpendingLimit: boolean
      weeklySpendingLimit: number
      currencyCode: string
      currencySymbol: string
      balance: number
      currentSpendWithinWeek: number
    }

    type ClientDebitCardMenuItem = {
      id?: number
      icon: ImageSourcePropType
      title: string
      subtitleWhenBooleanValueIsOff?: string
      subtitle: string
      hasBooleanStatus?: boolean
      booleanStatusValue?: boolean
      onTouch?: () => void
    }
  }
}
