import React from 'react'
import { TextStyle, View } from 'react-native'
import { FontText } from '@components/font-text'
import styles from './styles'

type CurrencyDecoratorProps = {
  currencyAlias: string
  value: string
  textColor?: string
  currencyDecoratorTitle?: TextStyle
}

const CurrencyDecorator = (props: CurrencyDecoratorProps) => {
  return (
    <View style={styles.balanceWrapper}>
      <View style={styles.currencyDecoratorView}>
        <FontText style={styles.currencyDecoratorTitle}>
          {props.currencyAlias ? props.currencyAlias : '--'}
        </FontText>
      </View>

      <FontText
        style={[
          styles.balanceText,
          props.currencyDecoratorTitle,
          { color: props.textColor }
        ]}
      >
        {props.value ? props.value : '--'}
      </FontText>
    </View>
  )
}

export default CurrencyDecorator
