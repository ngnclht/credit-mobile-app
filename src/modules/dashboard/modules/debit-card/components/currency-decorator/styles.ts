import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { SIZES, wp2dp } from '@sizes/sizes'
import { isIOS } from '@m_app/utils/info'

const styles = StyleSheet.create({
  balanceWrapper: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  currencyDecoratorView: {
    backgroundColor: THEMES.MAIN,
    width: wp2dp(9),
    height: wp2dp(5.5),
    borderRadius: wp2dp(1.2),
    alignItems: 'center',
    justifyContent: 'center'
  },
  currencyDecoratorTitle: {
    color: THEMES.TEXT_1,
    fontWeight: 'bold',
    marginTop: isIOS ? 2 : 0,
    fontSize: SIZES.UI_FONT_SIZE_2
  },
  balanceText: {
    fontSize: SIZES.UI_FONT_SIZE_6,
    color: THEMES.TEXT_1,
    fontWeight: 'bold',
    marginLeft: 7
  }
})

export default styles
