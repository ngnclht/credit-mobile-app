import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { hp2dp, SIZES, wp2dp } from '@sizes/sizes'

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: THEMES.BACKGROUND_1,
    flexDirection: 'row',
    height: hp2dp(7),
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    marginVertical: hp2dp(1)
  },
  container: {
    flex: 1,
    marginLeft: SIZES.UI_PADDING_MARGIN * 0.8
  },
  menuIcon: {
    height: wp2dp('9'),
    width: wp2dp('9'),
    resizeMode: 'contain',
    marginBottom: hp2dp(1.1)
  },
  title: {
    fontSize: SIZES.UI_FONT_SIZE_4,
    color: THEMES.TEXT_DEBIT_CARD_MENU_TITLE,
    marginBottom: hp2dp(0.5)
  },
  subTitle: {
    fontSize: SIZES.UI_FONT_SIZE_3,
    color: THEMES.TEXT_GRAY1,
    opacity: 0.4
  },
  switch: {
    transform: [{ scaleX: 0.7 }, { scaleY: 0.7 }],
    marginBottom: wp2dp(3)
  }
})

export default styles
