import React from 'react'
import { Image, Switch, TouchableOpacity, View } from 'react-native'
import { FontText } from '@components/font-text'
import styles from './styles'

type DebitActionMenuItemProps = DEBITCARD.ClientDebitCardMenuItem

const DebitActionMenuItem = (props: DebitActionMenuItemProps) => {
  return (
    <TouchableOpacity style={styles.wrapper} onPress={props.onTouch}>
      <Image source={props.icon} style={styles.menuIcon} />
      <View style={styles.container}>
        <FontText style={styles.title}>{props.title}</FontText>
        <FontText style={styles.subTitle}>{props.subtitle}</FontText>
      </View>
      {props.hasBooleanStatus ? (
        <Switch
          disabled={true}
          style={styles.switch}
          value={props.booleanStatusValue}
        />
      ) : null}
    </TouchableOpacity>
  )
}

export default DebitActionMenuItem
