export const menuItemAssets = {
  menu_deactivatated_card: require('./menu/deactivated-card.png'),
  menu_freeze_card: require('./menu/freeze-card.png'),
  menu_get_new_card: require('./menu/get-new-card.png'),
  menu_topup_account: require('./menu/topup-account.png'),
  menu_weekly_spending_limit: require('./menu/weekly-spending-with-bg.png')
}
