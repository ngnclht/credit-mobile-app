import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { hp2dp, SIZES, wp2dp } from '@sizes/sizes'
import { isIOS } from '@m_app/utils/info'

export const BORDER_RADIUS = 10

const brandLogoSizeRatio = {
  height: wp2dp(6),
  width: wp2dp(18.6)
}

const styles = StyleSheet.create({
  wrapper: {
    zIndex: 101
  },
  toggleCardNumberButton: {
    backgroundColor: THEMES.BACKGROUND_1,
    borderTopLeftRadius: BORDER_RADIUS,
    borderTopRightRadius: BORDER_RADIUS,
    marginRight: SIZES.UI_PADDING_MARGIN,
    marginBottom: -BORDER_RADIUS,
    height: wp2dp(9.4),
    marginLeft: wp2dp(58),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: BORDER_RADIUS
  },
  toggleCardNumberButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  toggleCardNumberButtonImage: {
    resizeMode: 'contain',
    height: wp2dp(5),
    width: wp2dp(5),
    tintColor: THEMES.MAIN,
    marginRight: 4
  },
  toggleCardNumberButtonTitle: {
    marginTop: isIOS ? 3 : 0,
    fontSize: SIZES.UI_FONT_SIZE_1,
    color: THEMES.MAIN,
    fontWeight: '600'
  },
  cardWrapper: {
    flex: 1,
    backgroundColor: THEMES.VISA_CARD_BACKGROUND,
    marginHorizontal: SIZES.UI_PADDING_MARGIN,
    borderRadius: BORDER_RADIUS,
    height: (wp2dp(100) - SIZES.UI_PADDING_MARGIN * 2) * 0.603
  },
  brandLogo: {
    position: 'absolute',
    right: SIZES.UI_PADDING_MARGIN,
    top: SIZES.UI_PADDING_MARGIN,
    resizeMode: 'contain',
    ...brandLogoSizeRatio
  },
  cardTypeLogo: {
    position: 'absolute',
    right: SIZES.UI_PADDING_MARGIN,
    bottom: SIZES.UI_PADDING_MARGIN,
    resizeMode: 'contain',
    height: wp2dp(5),
    width: wp2dp(15)
  },
  cardContainer: {
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    marginTop: SIZES.UI_PADDING_MARGIN + brandLogoSizeRatio.height
  },
  cardOwnerName: {
    fontSize: SIZES.UI_FONT_SIZE_5,
    color: THEMES.TEXT_1,
    fontWeight: 'bold',
    marginTop: isIOS ? wp2dp('5.5%') : wp2dp('5.3%'),
    marginBottom: wp2dp('6.5%')
  },
  cardNumberSegmentWrapper: {
    flexDirection: 'row',
    marginBottom: isIOS ? wp2dp('3.4%') : wp2dp('2.4%')
  },
  cardNumberSegmentContainter: {
    flexDirection: 'row',
    marginRight: wp2dp(4)
  },
  cardNumber: {
    fontSize: SIZES.UI_FONT_SIZE_4,
    color: THEMES.TEXT_1,
    fontWeight: '600',
    letterSpacing: wp2dp(0.7)
  },
  cardExpAndCCVContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  cardExpContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  cardExpAndCCVContainerWhenHideSensitive: {
    marginTop: -wp2dp(2),
  },
  cardExpAndCCVText: {
    fontSize: SIZES.UI_FONT_SIZE_4,
    color: THEMES.TEXT_1,
    fontWeight: '600'
  },
  expDate: {
    marginRight: wp2dp(8)
  },
  ccvBigger: {
    fontSize: SIZES.UI_FONT_SIZE_5,
    textAlign: 'center',
    paddingTop: wp2dp(2)
  },
  dot: {
    marginTop: isIOS ? hp2dp(0.2) : hp2dp(0.8),
    backgroundColor: THEMES.TEXT_1,
    height: isIOS ? wp2dp(2) : wp2dp(1.9),
    width: isIOS ? wp2dp(2) : wp2dp(1.9),
    borderRadius: wp2dp(1),
    marginHorizontal: wp2dp(0.8)
  }
})

export default styles
