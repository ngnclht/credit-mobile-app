import React, { useCallback } from 'react'
import { Image, TouchableOpacity, View } from 'react-native'
import { useTranslation } from 'react-i18next'
import _ from 'lodash'
import { FontText } from '@components/font-text'
import styles from './styles'
import { visaCardComponentAssets } from '@m_dashboard_debit_card/components/visa-card/assets'

type VisaCardComponentProps = {
  fullName: string
  accountNumber: string
  expDate: string
  ccv: string
  brand: string
  hideSensive: boolean
  setHideSensive: (v: boolean) => void
}

const VisaCardComponent = (props: VisaCardComponentProps) => {
  const { t } = useTranslation()

  const toogleHideSensive = useCallback(() => {
    props.setHideSensive(!props.hideSensive)
  }, [props.hideSensive])

  const accountNumberSegments = props.accountNumber.match(/.{1,4}/g)
  return (
    <View style={styles.wrapper}>
      <View style={styles.toggleCardNumberButton}>
        <TouchableOpacity
          style={styles.toggleCardNumberButtonContainer}
          onPress={toogleHideSensive}
        >
          <Image
            source={
              props.hideSensive
                ? visaCardComponentAssets.showCardNumberEye
                : visaCardComponentAssets.hideCardNumberEye
            }
            style={styles.toggleCardNumberButtonImage}
          />
          <FontText style={styles.toggleCardNumberButtonTitle}>
            {props.hideSensive
              ? t('dashboard.debitCard_show_card_number')
              : t('dashboard.debitCard_hide_card_number')}
          </FontText>
        </TouchableOpacity>
      </View>
      <View style={styles.cardWrapper}>
        <Image
          source={visaCardComponentAssets.aspireLogo}
          style={styles.brandLogo}
        />
        <View style={styles.cardContainer}>
          <FontText style={styles.cardOwnerName}>{props.fullName}</FontText>
          <View style={styles.cardNumberSegmentWrapper}>
            {accountNumberSegments?.map((val: string, index: number) =>
              props.hideSensive ? (
                <View key={index} style={styles.cardNumberSegmentContainter}>
                  {index < 3 ? (
                    _.range(0, 4).map((i: number) => (
                      <View key={i} style={styles.dot} />
                    ))
                  ) : (
                    <FontText style={styles.cardNumber}>{`${val}`}</FontText>
                  )}
                </View>
              ) : (
                <View key={index} style={styles.cardNumberSegmentContainter}>
                  <FontText style={styles.cardNumber}>{`${val}`}</FontText>
                </View>
              )
            )}
          </View>
          <View
            style={[
              styles.cardExpAndCCVContainer,
              props.hideSensive &&
                styles.cardExpAndCCVContainerWhenHideSensitive
            ]}
          >
            <FontText style={[styles.cardExpAndCCVText, styles.expDate]}>
              {`${t('dashboard.debitCard_exp_date')}: ${props.expDate}`}
            </FontText>

            <View style={[styles.cardExpContainer]}>
              <FontText style={styles.cardExpAndCCVText}>
                {`${t('dashboard.debitCard_ccv')}: `}
              </FontText>
              <FontText
                style={[
                  styles.cardExpAndCCVText,
                  props.hideSensive && styles.ccvBigger
                ]}
              >
                {`${props.hideSensive ? '***' : props.ccv}`}
              </FontText>
            </View>
          </View>
        </View>
        <Image
          source={visaCardComponentAssets.visaLogo}
          style={styles.cardTypeLogo}
        />
      </View>
    </View>
  )
}

export default VisaCardComponent
