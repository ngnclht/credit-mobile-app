import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { hp2dp, SIZES } from '@sizes/sizes'

const HEIGHT_OF_PROGRESS_BAR = hp2dp(1.6)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'transparent',
    marginTop: hp2dp(0.8),
    marginBottom: hp2dp(1.4)
  },
  textContainer: {
    flexDirection: 'row',
    marginHorizontal: SIZES.UI_PADDING_MARGIN,
    marginBottom: hp2dp(1),
    justifyContent: 'space-between'
  },
  title: {
    color: THEMES.TEXT_GRAY1
  },
  limitText: {
    color: THEMES.MAIN
  },
  totalText: {
    color: THEMES.TEXT_GRAY2
  },
  bar: {
    marginHorizontal: SIZES.UI_PADDING_MARGIN,
    borderRadius: 7,
    height: HEIGHT_OF_PROGRESS_BAR,
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  barOuter: {
    backgroundColor: THEMES.PROGRESS_BAR_OUTER,
    overflow: 'hidden'
  },
  barInner: {
    marginLeft: undefined,
    marginHorizontal: undefined,
    backgroundColor: THEMES.MAIN,
    width: 40,
    alignItems: 'flex-end',
    padding: 0,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0
  },
  endOfBarInner: {
    width: 20,
    height: HEIGHT_OF_PROGRESS_BAR,
    marginRight: -20
  }
})

export default styles
