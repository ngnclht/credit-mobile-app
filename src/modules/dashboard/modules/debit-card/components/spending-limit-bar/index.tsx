import React from 'react'
import { Image, SafeAreaView, View } from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'

import styles from './styles'
import { SIZES, wp2dp } from '@sizes/sizes'
import { spendingLimitBarAssets } from '@m_dashboard_debit_card/components/spending-limit-bar/assets'
import { appUtil_formatMoney } from '@utils/currency'

type SpendingLimitBarProps = {
  total: number | '--'
  limit: number | '--'
  currencySymbol: string
}

const outerWidth100Percent = wp2dp(100) - SIZES.UI_PADDING_MARGIN * 2

const SpendingLimitBar = (props: SpendingLimitBarProps) => {
  const { t } = useTranslation()
  const { limit, total, currencySymbol } = props
  const valid = typeof limit == 'number' && typeof total == 'number'
  const innerBarWidth = valid
    ? ((limit as number) / (total as number)) * outerWidth100Percent
    : 0
  return (
    <SafeAreaView style={styles.wrapper}>
      <View style={styles.textContainer}>
        <FontText style={styles.title}>
          {t('dashboard.debitCard_debit_card_spend_limit')}
        </FontText>
        <FontText>
          <FontText style={styles.limitText}>
            {valid
              ? appUtil_formatMoney(limit as number, true, currencySymbol, true)
              : limit}
          </FontText>
          <FontText style={styles.totalText}>{` | ${
            valid
              ? appUtil_formatMoney(total as number, true, currencySymbol, true)
              : total
          } 
          `}</FontText>
        </FontText>
      </View>

      <View style={[styles.bar, styles.barOuter]}>
        <View style={[styles.bar, styles.barInner, { width: innerBarWidth }]}>
          <Image
            source={spendingLimitBarAssets.thumbImage}
            style={styles.endOfBarInner}
          />
        </View>
      </View>
    </SafeAreaView>
  )
}

export default SpendingLimitBar
