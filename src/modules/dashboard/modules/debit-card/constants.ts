// we will define constants here

import { menuItemAssets } from '@m_dashboard_debit_card/components/menu-item/assets'
import { navService_Push } from '@m_app/utils/navigation/navigation-services'

import { Alert } from 'react-native'
import { i18n } from '@m_app/registry/i18n'
import { ScreenKeys } from '@m_app/registry/screen-keys'

export const WEEKLY_SPENDING_MENU_ID = 1
export const FREEZE_CARD_MENU_ID = 2

export const DEBITCARD_ACTION_MENU: DEBITCARD.ClientDebitCardMenuItem[] = [
  {
    title: i18n.t('dashboard.debitCard_menu_topup_account'),
    subtitle: i18n.t('dashboard.debitCard_menu_topup_account_subtitle'),
    icon: menuItemAssets.menu_topup_account,
    onTouch: () => Alert.alert(i18n.t('dashboard.debitCard_feature_missing'))
  },
  {
    title: i18n.t('dashboard.debitCard_menu_weekly_spending_limit'),
    subtitle: i18n.t('dashboard.debitCard_menu_weekly_spending_limit_subtitle'),
    subtitleWhenBooleanValueIsOff: i18n.t(
      'dashboard.debitCard_menu_weekly_spending_limit_subtitle_no_limit'
    ),
    icon: menuItemAssets.menu_weekly_spending_limit,
    hasBooleanStatus: true,
    id: WEEKLY_SPENDING_MENU_ID,
    onTouch: () => navService_Push(ScreenKeys.SCREEN_SET_WEEKLY_SPENDING_LIMIT)
  },
  {
    title: i18n.t('dashboard.debitCard_menu_freeze_card'),
    subtitle: i18n.t('dashboard.debitCard_menu_freeze_card_subtitle'),
    icon: menuItemAssets.menu_freeze_card,
    hasBooleanStatus: true,
    id: FREEZE_CARD_MENU_ID,
    onTouch: () => Alert.alert(i18n.t('dashboard.debitCard_feature_missing'))
  },
  {
    title: i18n.t('dashboard.debitCard_menu_get_new_card'),
    subtitle: i18n.t('dashboard.debitCard_menu_get_new_card_subtitle'),
    icon: menuItemAssets.menu_get_new_card,
    onTouch: () => Alert.alert(i18n.t('dashboard.debitCard_feature_missing'))
  },
  {
    title: i18n.t('dashboard.debitCard_menu_deactivatated_card'),
    subtitle: i18n.t('dashboard.debitCard_menu_deactivatated_card_subtitle'),
    icon: menuItemAssets.menu_deactivatated_card,
    onTouch: () => Alert.alert(i18n.t('dashboard.debitCard_feature_missing'))
  }
]

// this should be dynamic from server side
export const DEBITCARD_LIMIT_LEVEL = [5000, 10000, 20000]
