import { hp2dp, SIZES } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: THEMES.BACKGROUND_1
  },
  bookList: {
    flex: 1,
    padding: SIZES.UI_PADDING_MARGIN,
    marginBottom: hp2dp(2)
  },
  title: {
    marginVertical: hp2dp(1),
    fontSize: SIZES.UI_FONT_SIZE_6,
    fontWeight: '600',
    paddingHorizontal: SIZES.UI_PADDING_MARGIN,
    marginBottom: hp2dp(1),
    color: THEMES.TEXT_1
  }
})

export default styles
