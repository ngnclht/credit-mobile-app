export const dashboardAssets = {
  tabIcons: {
    home: require('./tab-icons/home-tab.png'),
    credit: require('./tab-icons/credit-tab.png'),
    debitCard: require('./tab-icons/debit-tab-icon.png'),
    payment: require('./tab-icons/payment-tab-icon.png'),
    profile: require('./tab-icons/profile-tab.png')
  }
}
