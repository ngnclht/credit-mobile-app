import { hp2dp, SIZES, wp2dp } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { isIOS } from '@m_app/utils/info'

const styles = StyleSheet.create({
  title: {
    fontSize: SIZES.UI_FONT_SIZE_1,
    fontWeight: '300',
    textAlign: 'center',
    marginBottom: isIOS ? 0 : hp2dp(1)
  },
  icon: {
    height: wp2dp(5),
    width: wp2dp(5),
    resizeMode: 'contain'
  },
  tabBarStyle: {
    backgroundColor: THEMES.BACKGROUND_1,
    height: isIOS ? hp2dp(10) : hp2dp(8),
    justifyContent: 'center'
  }
})

export default styles
