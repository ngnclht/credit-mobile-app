import * as React from 'react'
import { Image, SafeAreaView } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { DashboardScreenKeys } from '@m_dashboard/screens/keys'
import THEMES from '@themes/theme'
import { useTranslation } from 'react-i18next'
import { hp2dp } from '@sizes/sizes'
import ProfileTab from '@m_dashboard_profile/screens/tab-profile'
import HomeTab from '@m_dashboard_home/screens/tab-home'
import DebitCardTab from '@m_dashboard_debit_card/screens/tab-debit-card'
import PaymentTab from '@m_dashboard_payment/screens/tab-payment'
import CreditTab from '@m_dashboard_credit/screens/tab-credit'
import { dashboardAssets } from '@m_dashboard/assets'
import styles from '@m_dashboard/screens/dashboard/styles'

const Tab = createBottomTabNavigator()

export default function App() {
  const { t } = useTranslation()
  const tabs = {
    home: t('dashboard.tab_home'),
    debit: t('dashboard.tab_debit_card'),
    payment: t('dashboard.tab_payment'),
    credit: t('dashboard.tab_credit'),
    profile: t('dashboard.tab_profile')
  }
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let icon
          if (route.name === tabs.home) {
            icon = dashboardAssets.tabIcons.home
          } else if (route.name === tabs.credit) {
            icon = dashboardAssets.tabIcons.credit
          } else if (route.name === tabs.debit) {
            icon = dashboardAssets.tabIcons.debitCard
          } else if (route.name === tabs.payment) {
            icon = dashboardAssets.tabIcons.payment
          } else {
            icon = dashboardAssets.tabIcons.profile
          }
          return (
            <Image
              style={[
                styles.icon,
                { tintColor: focused ? THEMES.TAB_ACTIVE : THEMES.TAB_INACTIVE }
              ]}
              source={icon}
            />
          )
        },
        tabBarActiveTintColor: THEMES.TAB_ACTIVE,
        tabBarInactiveTintColor: THEMES.TAB_INACTIVE,
        tabBarStyle: styles.tabBarStyle,
        tabBarLabelStyle: styles.title,
        tabBarLabelPosition: 'below-icon'
      })}
    >
      <Tab.Screen
        name={tabs.home}
        key={tabs.home}
        component={HomeTab}
        options={{ header: () => null }}
      />
      <Tab.Screen
        name={tabs.debit}
        key={DashboardScreenKeys.TAB_DEBIT_CARD}
        component={DebitCardTab}
        options={{ header: () => null }}
      />
      <Tab.Screen
        name={tabs.payment}
        key={DashboardScreenKeys.TAB_PAYMENT}
        component={PaymentTab}
        options={{ header: () => null }}
      />
      <Tab.Screen
        name={tabs.credit}
        key={DashboardScreenKeys.TAB_CREDIT}
        component={CreditTab}
        options={{ header: () => null }}
      />
      <Tab.Screen
        name={tabs.profile}
        key={DashboardScreenKeys.TAB_PROFILE}
        component={ProfileTab}
        options={{ header: () => null }}
      />
    </Tab.Navigator>
  )
}
