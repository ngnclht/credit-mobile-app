export const DashboardScreenKeys = {
  DASHBOARD: 'DASHBOARD',
  TAB_PROFILE: 'TAB_PROFILE',
  TAB_HOME: 'TAB_HOME',
  TAB_CREDIT: 'TAB_CREDIT',
  TAB_DEBIT_CARD: 'TAB_DEBIT_CARD',
  TAB_PAYMENT: 'TAB_PAYMENT',
  SCREEN_SET_WEEKLY_SPENDING_LIMIT: 'SCREEN_SET_WEEKLY_SPENDING_LIMIT'
}
