import { appActions } from '@m_app/reducers/app.reducer'
import { PayloadAction } from '@reduxjs/toolkit'
import { takeEvery } from 'redux-saga/effects'

function* appStateChangeFlow(action: PayloadAction<string>) {
  console.log('appStateChangeFlow', action)
}

export function* appStateChangeWatcher() {
  yield takeEvery(appActions.actApp_ChangeState.type, appStateChangeFlow)
}
