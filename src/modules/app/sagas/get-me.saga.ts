import { apiGetMe } from '@m_app/api/auth'
import { authActions } from '@m_app/reducers/auth.reducer'
import { PayloadAction } from '@reduxjs/toolkit'
import { call, put, takeEvery } from 'redux-saga/effects'

function* getMeFlow(action: PayloadAction<string>) {
  try {
    const result = (yield call(apiGetMe)) as APP.Me
    yield put(authActions.getMeSuccess(result))
  } catch (error: any) {
    yield put(authActions.getMeFailed(error.message))
  }
}

export function* getMeWatcher() {
  yield takeEvery(authActions.getMeRequest.type, getMeFlow)
}
