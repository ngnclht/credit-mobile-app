import { apiLogin } from '@m_app/api/auth'
import { authActions } from '@m_app/reducers/auth.reducer'
import { authStorages } from '@m_app/storages/auth.storage'
import { PayloadAction } from '@reduxjs/toolkit'
import { call, put, takeEvery } from 'redux-saga/effects'

function* loginFlow(
  action: PayloadAction<{ username: string; password: string }>
) {
  try {
    const result = (yield apiLogin(
      action.payload.username,
      action.payload.password
    )) as APP.LoginResult
    yield authStorages.saveAccessToken(result.access_token)
    yield put(authActions.loginSuccess(result.access_token))
    yield put(authActions.startPostLoginFlow())
  } catch (error: any) {
    yield put(authActions.loginFailed(error.message))
  }
}

export function* loginWatcher() {
  yield takeEvery(authActions.loginRequest.type, loginFlow)
}
