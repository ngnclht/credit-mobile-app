import { appActions } from '@m_app/reducers/app.reducer'
import { authActions } from '@m_app/reducers/auth.reducer'
import { ScreenKeys } from '@m_app/registry/screen-keys'
import { authStorages } from '@m_app/storages/auth.storage'
import { navService_Replace } from '@m_app/utils/navigation/navigation-services'
import { PayloadAction } from '@reduxjs/toolkit'
import { call, put, takeLeading } from 'redux-saga/effects'

function* initFlow(action: PayloadAction<undefined>) {
  const accessToken = (yield authStorages.loadAccessToken()) as
    | string
    | undefined
  if (!accessToken) yield nonLogged()
  else yield logged(accessToken)
}

function* nonLogged() {
  navService_Replace(ScreenKeys.ONBOARDING)
}

function* logged(accessToken: string) {
  yield put(authActions.setAccessToken(accessToken))
  yield put(authActions.startPostLoginFlow())
}

export function* initWatcher() {
  yield takeLeading(appActions.actApp_Init.type, initFlow)
}
