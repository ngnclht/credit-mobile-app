import { authActions } from '@m_app/reducers/auth.reducer'
import { ScreenKeys } from '@m_app/registry/screen-keys'
import { navService_Reset } from '@m_app/utils/navigation/navigation-services'
import { PayloadAction } from '@reduxjs/toolkit'
import { Alert } from 'react-native'
import { put, take, takeEvery } from 'redux-saga/effects'
import { debitCardPostLogin } from '@m_dashboard_debit_card/sagas/post-login.saga'

function* postLoginFlow(_action: PayloadAction<undefined>) {
  yield put(authActions.getMeRequest())
  const result = (yield take([
    authActions.getMeFailed.type,
    authActions.getMeSuccess.type
  ])) as PayloadAction<any>
  if (result.type === authActions.getMeFailed.type) {
    Alert.alert('Error ')
    return
  }

  navService_Reset(ScreenKeys.DASHBOARD)

  // run post login task for each module if needed
  yield debitCardPostLogin()
}

export function* postLoginWatcher() {
  yield takeEvery(authActions.startPostLoginFlow.type, postLoginFlow)
}
