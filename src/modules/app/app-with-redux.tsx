import '@utils/number'
import React, { useEffect } from 'react'
import { Provider, useDispatch } from 'react-redux'
import { View, StatusBar } from 'react-native'
import { ActionSheetProvider } from '@expo/react-native-action-sheet'
import { AppPersistor, ReduxStore } from '@m_app/configs/store.config'
import Screens from '@m_app/registry/screens'
import { NavigationContainer } from '@react-navigation/native'
import { appActions } from './reducers/app.reducer'
import { navigationRef } from './utils/navigation/navigation-services'
import { PersistGate } from 'redux-persist/integration/react'

export const AppWithRedux = () => (
  <PersistGate loading={null} persistor={AppPersistor}>
    <Provider store={ReduxStore}>
      <App />
    </Provider>
  </PersistGate>
)

const App = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(appActions.actApp_Init())
    return () => {}
  }, [])

  return (
    <View style={{ flex: 1 }}>
      <StatusBar barStyle="dark-content" />
      <ActionSheetProvider>
        <NavigationContainer ref={navigationRef}>
          <Screens />
        </NavigationContainer>
      </ActionSheetProvider>
    </View>
  )
}

module.exports = AppWithRedux
