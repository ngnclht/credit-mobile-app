import React from 'react'
import {
  ActivityIndicator,
  StyleSheet,
  Platform,
  Alert,
  TouchableOpacity
} from 'react-native'
import ProgressCircle from 'react-native-progress-circle'
import codePush from 'react-native-code-push'
import DeviceInfo from 'react-native-device-info'
import semver from 'semver'
import Splash from '@m_app/screens/splash'
import { FontText } from '@components/font-text'
import { openStore } from '@m_app/utils/links'
import { isIOS } from '@m_app/utils/info'
import AppGlobal from '@/app-global'
import { useTranslation } from 'react-i18next'
import THEMES from '@themes/theme'

const versionManagement = require('../../../version-management')

const { OKVersion } = versionManagement

interface State {
  percents: number
  status: any
  content: any
  canContinue: boolean
}

const styles = StyleSheet.create({
  update: {
    position: 'absolute',
    bottom: 20,
    color: THEMES.TEXT_GRAY1
  },
  txtAlign: {
    textAlign: 'center'
  }
})

// @ts-ignore
class CodePushWrapper extends React.Component<any, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      canContinue: false,
      status: codePush.SyncStatus.CHECKING_FOR_UPDATE,
      percents: 0,
      content: null
    }
  }

  private componentDidMount = async () => {
    const { t } = this.props
    const currentVersion = await DeviceInfo.getVersion()

    const okVersions = AppGlobal.isApp1()
      ? isIOS
        ? OKVersion.app1.ios
        : OKVersion.app1.android
      : isIOS
      ? OKVersion.app2.ios
      : OKVersion.app2.android

    const ok = semver.satisfies(currentVersion, okVersions)
    console.log({ currentVersion, okVersions })

    if (!ok)
      Alert.alert(t('app.update_needed'), t('app.update_we_have_fixed_bug'), [
        { text: t('app.update_ok'), onPress: openStore }
      ])

    this.setState({
      canContinue: ok
    })
  }

  codePushStatusDidChange(status: number) {
    const { t } = this.props
    this.setState({ status })
    const { percents } = this.state
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        this.setState({
          content: (
            <FontText style={[styles.update, styles.txtAlign]}>
              {t('app.check_for_updates')}
            </FontText>
          )
        })
        break
      case codePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState({
          content: <ActivityIndicator style={styles.update} color="#e1e1e1" />
        })
        break
      case codePush.SyncStatus.INSTALLING_UPDATE:
        console.log('codePush Installing update.')
        this.setState({
          content: (
            <ProgressCircle
              percent={percents}
              radius={10}
              borderWidth={8}
              color="#60B5F6"
              shadowColor="white"
              bgColor="transparent"
              containerStyle={styles.update}
            />
          )
        })
        break
      case codePush.SyncStatus.UPDATE_INSTALLED:
        console.log('codePush Update installed.')
        if (!isIOS) codePush.restartApp()
        break
      default:
        this.setState({
          content: null
        })
        break
    }
  }

  codePushDownloadDidProgress(progress: any) {
    console.log(
      progress.receivedBytes + ' of ' + progress.totalBytes + ' received.'
    )
    this.setState({
      percents: Math.floor((progress.receivedBytes / progress.totalBytes) * 100)
    })
  }

  render() {
    const { content, status, canContinue } = this.state
    const { t } = this.props
    if (
      canContinue &&
      (status === codePush.SyncStatus.UP_TO_DATE ||
        status === codePush.SyncStatus.UPDATE_IGNORED ||
        status === codePush.SyncStatus.UNKNOWN_ERROR)
    ) {
      const App = require('./app-with-redux')
      return <App />
    }
    return (
      <Splash>
        {canContinue ? (
          content
        ) : (
          <>
            <FontText style={[styles.update, styles.txtAlign]}>
              {t('app.please_update_your_app')}
            </FontText>

            <TouchableOpacity onPress={openStore}>
              <FontText style={[styles.update, styles.txtAlign]}>
                {t('app.please_update_your_app')}
              </FontText>
            </TouchableOpacity>
          </>
        )}
      </Splash>
    )
  }
}

// @ts-ignore
const CodePushWithTranslation = codePush({
  installMode: codePush.InstallMode.IMMEDIATE
})(CodePushWrapper)

const CodePushEndpoint = (props: any) => {
  const { t } = useTranslation()
  // @ts-ignore
  return <CodePushWithTranslation {...props} t={t} />
}

export default CodePushEndpoint
