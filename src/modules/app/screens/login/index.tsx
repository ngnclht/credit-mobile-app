import React, { useCallback, useState } from 'react'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import {
  Alert,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'
import { authActions, authSelectors } from '@m_app/reducers/auth.reducer'
import { appDispatchAction } from '@m_app/utils/redux/dispatch-action'
import { useSelector } from 'react-redux'

import styles from './styles'

const LoginScreen = (props: any) => {
  const { t } = useTranslation()

  const isLoggingIn = useSelector(authSelectors.isLoggingIn)
  const [UserName, setUserName] = useState<string | undefined>()
  const [Password, setPassword] = useState<string | undefined>()

  const loginRequest = appDispatchAction(
    authActions.loginRequest({ username: UserName, password: Password })
  )

  const onSubmit = useCallback(() => {
    if (!UserName || !Password)
      return Alert.alert(t('app.auth_enter_user_name_password'))
    loginRequest()
  }, [UserName, Password])

  return (
    <SafeAreaView style={styles.wrapper}>
      <KeyboardAwareScrollView>
        <FontText style={styles.title}>{t('app.login_title')}</FontText>
        {props.children}
        <TextInput
          style={styles.textInput}
          placeholder={t('app.typeAnything')}
          onChangeText={(text: string) => setUserName(text)}
        />

        <TextInput
          style={styles.textInput}
          placeholder={t('app.typeAnything')}
          onChangeText={(text: string) => setPassword(text)}
        />

        <TouchableOpacity onPress={onSubmit} disabled={isLoggingIn}>
          {isLoggingIn ? (
            <ActivityIndicator />
          ) : (
            <FontText style={styles.title}>
              {t('app.login_submit_btn')}
            </FontText>
          )}
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  )
}

export default LoginScreen
