import { hp2dp, SIZES, wp2dp } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: SIZES.UI_PADDING_MARGIN,
    backgroundColor: THEMES.BACKGROUND_1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: SIZES.UI_FONT_SIZE_6,
    fontWeight: '600',
    color: THEMES.TEXT_2,
    textAlign: 'center'
  },
  subTitle: {
    fontSize: SIZES.UI_FONT_SIZE_1,
    fontWeight: '300',
    textAlign: 'center',
    color: THEMES.TEXT_1
  },
  textInput: {
    backgroundColor: THEMES.BACKGROUND_3,
    borderRadius: 5,
    height: hp2dp(5.1),
    width: wp2dp(80),
    marginVertical: hp2dp(1)
  }
})

export default styles
