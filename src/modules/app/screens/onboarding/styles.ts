import { hp2dp, SIZES } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: SIZES.UI_PADDING_MARGIN,
    backgroundColor: THEMES.BACKGROUND_1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: SIZES.UI_FONT_SIZE_6,
    fontWeight: '600',
    color: THEMES.TEXT_2,
    textAlign: 'center',
    marginBottom: hp2dp(2)
  },
  subTitle: {
    fontSize: SIZES.UI_FONT_SIZE_2,
    fontWeight: '300',
    color: THEMES.TEXT_2
  }
})

export default styles
