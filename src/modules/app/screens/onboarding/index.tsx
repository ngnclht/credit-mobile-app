import React from 'react'
import { SafeAreaView, TouchableOpacity } from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'

import styles from './styles'
import { appNavigateTo } from '@m_app/utils/navigation/navigate-to'
import { ScreenKeys } from '@m_app/registry/screen-keys'

const OnBoarding = (props: any) => {
  const { t } = useTranslation()
  const navigateToLoginPage = appNavigateTo(ScreenKeys.LOGIN)

  return (
    <SafeAreaView style={styles.wrapper}>
      <FontText style={styles.title}>{t('app.onboarding')}</FontText>
      {props.children}

      <TouchableOpacity onPress={navigateToLoginPage}>
        <FontText style={styles.title}>
          {t('app.onboarding_login_btn')}
        </FontText>
      </TouchableOpacity>
    </SafeAreaView>
  )
}

export default OnBoarding
