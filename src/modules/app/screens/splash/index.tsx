import React from 'react'
import { SafeAreaView, ActivityIndicator } from 'react-native'
import { useTranslation } from 'react-i18next'
import { FontText } from '@components/font-text'

import styles from './styles'

const SplashScreen = (props: any) => {
  const { t } = useTranslation()

  return (
    <SafeAreaView style={styles.wrapper}>
      <FontText style={styles.title}>{t('app.hello')}</FontText>
      <FontText style={styles.subTitle}>{t('app.hello')}</FontText>
      {props.children}
      <ActivityIndicator />
    </SafeAreaView>
  )
}

export default SplashScreen
