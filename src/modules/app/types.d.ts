import { ReduxStore } from './configs/store.config'
import { RootReducer } from "@m_app/registry/reducer";

declare global {
  namespace APP {
    type RootState = ReturnType<typeof RootReducer>
    type AppDispatch = typeof ReduxStore.dispatch

    type RG = {
      config?: any
      data?: any
      headers?: any
      request?: any
      status?: number
      statusText?: string
    }

    type AppModal = {
      name: string
    }

    type AppConfig = {
      SENTRY_URL: string
      ANDROID_APP_ID: string
      IOS_APP_ID: string
      PASSWORD: string
      BUNDLE_ID: string
      API_URL: string
      APP: 'endUser' | 'contentEditor'
      IS_PRODUCTION: string
    }

    type Me = {
      id: number
      first_name?: string
      last_name?: string
      email?: string
      avatar?: string
      access_token?: string
    }

    type LoginResult = {
      access_token: string
    }

    type AppState = {
      appCurrentState: string
      appStateHistory: string[]
    }

    type AuthState = {
      me?: Me
      loggingIn: boolean
      error?: string
      accessToken?: string
    }
  }
}
