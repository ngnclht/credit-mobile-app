import { AppScreenKeys } from '@m_app/screens/keys'
import { DashboardScreenKeys } from '@m_dashboard/screens/keys'

export const ScreenKeys = {
  ...AppScreenKeys,
  ...DashboardScreenKeys
}
