import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import AppModuleSplash from '@m_app/screens/splash'
import AppModuleOnboarding from '@m_app/screens/onboarding'
import LoginScreen from '@m_app/screens/login'
import DashboardScreen from '@m_dashboard/screens/dashboard'
import SetWeeklySpendingLimitScreen from '@m_dashboard_debit_card/screens/set-weekly-spending-limit'

import { ScreenKeys } from './screen-keys'

const { Navigator, Screen } = createNativeStackNavigator()

function StackNavigator() {
  return (
    <Navigator initialRouteName={ScreenKeys.SPLASH}>
      <Screen
        name={ScreenKeys.SPLASH}
        component={AppModuleSplash}
        options={{ header: () => null }}
      />
      <Screen
        name={ScreenKeys.ONBOARDING}
        component={AppModuleOnboarding}
        options={{ header: () => null }}
      />
      <Screen
        name={ScreenKeys.LOGIN}
        getComponent={() => LoginScreen}
        options={{ header: () => null }}
      />
      <Screen
        name={ScreenKeys.DASHBOARD}
        getComponent={() => DashboardScreen}
        options={{ header: () => null }}
      />
      <Screen
        name={ScreenKeys.SCREEN_SET_WEEKLY_SPENDING_LIMIT}
        getComponent={() => SetWeeklySpendingLimitScreen}
        options={{ header: () => null }}
      />
    </Navigator>
  )
}

export default StackNavigator
