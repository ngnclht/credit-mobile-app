import { appStateChangeWatcher } from '@m_app/sagas/app-state.sagas'
import { getMeWatcher } from '@m_app/sagas/get-me.saga'
import { initWatcher } from '@m_app/sagas/init.sagas'
import { loginWatcher } from '@m_app/sagas/login.saga'
import { postLoginWatcher } from '@m_app/sagas/post-login.saga'
import { all, fork } from 'redux-saga/effects'
import { fetchDebitCardWatcher } from '@m_dashboard_debit_card/sagas/fetchDebitCard.saga'
import { setWeelyLimitForDebitCardWatcher } from "@m_dashboard_debit_card/sagas/set-weekly-limit-for-credit-card.saga";

export function* RootSagas() {
  yield all([
    fork(initWatcher),
    fork(appStateChangeWatcher),
    fork(loginWatcher),
    fork(postLoginWatcher),
    fork(getMeWatcher),
    fork(fetchDebitCardWatcher),
    fork(setWeelyLimitForDebitCardWatcher)
  ])
}
