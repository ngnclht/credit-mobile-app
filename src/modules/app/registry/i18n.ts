// this will be run in very first, please don't import anything regarding to
// navigation and redux here
import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'
import appLang from '@m_app/i18n'
import dashboardLang from '@m_dashboard/i18n'

const languageDetector = {
  type: 'languageDetector',
  async: true,
  detect: (cb: any) => cb('en'),
  init: () => {},
  cacheUserLanguage: () => {}
}

i18next
  // @ts-ignore
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    debug: __DEV__,
    resources: {
      en: {
        translation: {
          app: appLang.en,
          dashboard: dashboardLang.en
        }
      },
      vi: {
        translation: {
          app: appLang.vi,
          dashboard: dashboardLang.vi
        }
      }
    }
  })

export const i18n = i18next
