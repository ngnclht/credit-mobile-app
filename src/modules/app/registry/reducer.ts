import { appReducer } from '@m_app/reducers/app.reducer'
import { authReducer } from '@m_app/reducers/auth.reducer'
import { combineReducers } from '@reduxjs/toolkit'
import { debitCardReducer } from '@m_dashboard_debit_card/reducers/debit-card.reducer'

export const RootReducer = combineReducers({
  app: appReducer.reducer,
  auth: authReducer.reducer,
  debitCard: debitCardReducer.reducer
})
