import { RootReducer } from '@m_app/registry/reducer'
import { RootSagas } from '@m_app/registry/saga'
import { configureStore } from '@reduxjs/toolkit'
import {
  FLUSH,
  PAUSE,
  PERSIST,
  persistReducer,
  persistStore,
  PURGE,
  REGISTER,
  REHYDRATE
} from 'redux-persist'
import Reactoron from '@m_app/configs/reactoron.config'
import createSagaMiddleware from 'redux-saga'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { AppConfig } from '@m_app/configs/env.config'
import { encryptTransform } from 'redux-persist-transform-encrypt'
import { Alert } from 'react-native'

const sagaMiddleware = createSagaMiddleware()
export var AppPersistor: any = null

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['debitCard'],
  transforms: [
    encryptTransform({
      secretKey: AppConfig.PASSWORD,
      onError: function (error) {
        // TODO Handle the error.
        Alert.alert(error.message)
      }
    })
  ]
}

const persistedReducer: any = persistReducer(persistConfig, RootReducer)

const isDev = __DEV__
const isTest = typeof jest != 'undefined'

export const ReduxStore = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => {
    const mdws = getDefaultMiddleware({
      thunk: false,
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
      }
    })
    return [...mdws, sagaMiddleware]
  },
  devTools: !isDev && !isTest,
  preloadedState: {},
  enhancers: !isDev && !isTest ? [Reactoron.createEnhancer()] : []
})

AppPersistor = persistStore(ReduxStore)

sagaMiddleware.run(RootSagas)
