import Config from 'react-native-config'

// @ts-ignore
const config: APP.AppConfig = Config

export const AppConfig: APP.AppConfig = {
  ...config,
  API_URL: Config.API_URL
}
