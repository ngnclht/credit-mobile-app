import { NativeModules } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Reactotron from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'

let scriptHostname
let Reactoron: any = {}
if (__DEV__) {
  const scriptURL = NativeModules.SourceCode.scriptURL
  scriptHostname = scriptURL.split('://')[1].split(':')[0]
  // @ts-ignore
  if (typeof jest == 'undefined') console.log = Reactotron.log
  // @ts-ignore
  Reactoron = Reactotron.setAsyncStorageHandler(AsyncStorage) // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from
    .configure({ host: scriptHostname })
    .use(reactotronRedux())
    .useReactNative() // add all built-in react native plugins
    .connect() // let's connect!
}

export const showLoadedModule = () => {
  // @ts-ignore
  const modules = require.getModules()
  const moduleIds = Object.keys(modules)
  const loadedModuleNames = moduleIds
    .filter((moduleId) => modules[moduleId].isInitialized)
    .map((moduleId) => modules[moduleId].verboseName)
  const waitingModuleNames = moduleIds
    .filter((moduleId) => !modules[moduleId].isInitialized)
    .map((moduleId) => modules[moduleId].verboseName)

  // make sure that the modules you expect to be waiting are actually waiting
  console.log(
    'loaded:',
    loadedModuleNames.length,
    'waiting:',
    waitingModuleNames.length
  )

  // grab this text blob, and put it in a file named packager/modulePaths.js
  console.log(
    `module.exports = ${JSON.stringify(loadedModuleNames.sort(), null, 2)};`
  )
}

export default Reactoron
