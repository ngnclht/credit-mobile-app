import { AsyncStorage } from '@/storages/encrypted-async-storage'

const ACCESS_TOKEN = 'app-v1-accessToken'
const FIRST_TIME_OPENED = 'app-v1-firstTimeOpen'

async function saveAccessToken(accessToken: string) {
  return await AsyncStorage.setItem(ACCESS_TOKEN, accessToken)
}

async function loadAccessToken(): Promise<string | undefined> {
  return await AsyncStorage.getItem(ACCESS_TOKEN)
}

async function removeAccessToken() {
  return await AsyncStorage.removeItem(ACCESS_TOKEN)
}

async function isFirstTime(): Promise<boolean> {
  return !!(await AsyncStorage.getItem(FIRST_TIME_OPENED))
}

async function clearFirstTime() {
  return await AsyncStorage.setItem(FIRST_TIME_OPENED, 'true')
}

export const authStorages = {
  saveAccessToken,
  loadAccessToken,
  removeAccessToken,
  isFirstTime,
  clearFirstTime
}
