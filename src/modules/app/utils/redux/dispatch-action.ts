import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { PayloadAction } from '@reduxjs/toolkit'

export const appDispatchAction = (action: PayloadAction<any>) => {
  const dispatch = useDispatch()

  const fnc = useCallback(() => {
    dispatch(action)
  }, [action])

  return fnc
}
