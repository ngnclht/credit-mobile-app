import { useNavigation } from '@react-navigation/native'
import { useCallback } from 'react'
import { StackActions } from '@react-navigation/native'

export const appPopToTop = () => {
  const navigator = useNavigation()

  const fnc = useCallback(() => {
    navigator.dispatch(StackActions.popToTop())
  }, [])

  return fnc
}
