import {
  CommonActions,
  createNavigationContainerRef,
  StackActions
} from '@react-navigation/native'
import { doGoback } from './go-back'

export const navigationRef = createNavigationContainerRef()

export function navService_Navigate(name: string, params?: any) {
  if (navigationRef.isReady()) {
    // @ts-ignore
    navigationRef.navigate(name, params)
  }
}

export function navService_Push(key: string, params?: any) {
  if (navigationRef.isReady()) {
    navigationRef.dispatch(StackActions.push(key, params))
  }
}

export function navService_Replace(key: string, params?: any) {
  if (navigationRef.isReady()) {
    navigationRef.dispatch(StackActions.replace(key, params))
  }
}

export function navService_Reset(key: string) {
  if (navigationRef.isReady()) {
    navigationRef.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: key }]
      })
    )
  }
}

export function navService_GoBack() {
  if (navigationRef.isReady()) {
    doGoback(navigationRef)
  }
}

export function navService_Pop(n: number) {
  if (navigationRef.isReady()) {
    navigationRef.dispatch(StackActions.pop(n))
  }
}
