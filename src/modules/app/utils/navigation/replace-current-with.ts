import { useNavigation } from '@react-navigation/native'
import { useCallback } from 'react'
import { StackActions } from '@react-navigation/native'

export const appReplaceCurrentWith = (key: string, params?: object) => {
  const navigator = useNavigation()

  const fnc = useCallback(() => {
    navigator.dispatch(StackActions.replace(key, params))
  }, [])

  return fnc
}
