import { useNavigation } from '@react-navigation/native'
import { useCallback } from 'react'

export const appNavigateTo = (key: string, params?: object) => {
  const navigator = useNavigation()

  const fnc = useCallback(() => {
    // @ts-ignore
    navigator.navigate(key, params)
  }, [])

  return fnc
}
