import { useNavigation } from '@react-navigation/native'
import { useCallback } from 'react'
import { Alert } from 'react-native'

export const appGoBack = () => {
  const navigator = useNavigation()

  const fnc = useCallback(() => {
    // @ts-ignore
    doGoback(navigator)
  }, [])

  return fnc
}

export const doGoback = (navigator: any) => {
  if (navigator.canGoBack()) navigator.goBack()
  else Alert.alert("Can't go back")
}
