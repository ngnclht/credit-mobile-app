import { AppConfig } from '@m_app/configs/env.config'
import { Linking, Platform } from 'react-native'

const isIOS = Platform.OS === 'ios'

export const openUrl = (url: string) => {
  Linking.openURL(url).catch((err) => console.error('An error occurred', err))
}

export const openStore = () => {
  try {
    Linking.openURL(
      isIOS
        ? `itms-apps://itunes.apple.com/us/app/${AppConfig.IOS_APP_ID}?mt=8`
        : `market://details?id=${AppConfig.ANDROID_APP_ID}`
    )
  } catch (error) {}
}
