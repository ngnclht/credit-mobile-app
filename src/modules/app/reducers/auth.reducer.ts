import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState = {
  accessToken: undefined,
  me: undefined,
  error: undefined,
  loggingIn: false
} as APP.AuthState

export const authReducer = createSlice({
  name: 'app/auth',
  initialState,
  reducers: {
    loginRequest(
      state,
      action: PayloadAction<{ username?: string; password?: string }>
    ) {
      state.loggingIn = true
    },
    loginFailed(state, action: PayloadAction<string>) {
      state.error = action?.payload
      state.loggingIn = false
    },
    loginSuccess(state, action: PayloadAction<string>) {
      state.error = undefined
      state.accessToken = action?.payload
    },
    setAccessToken(state, action: PayloadAction<string>) {
      state.accessToken = action?.payload
    },
    startPostLoginFlow(state, action: PayloadAction<undefined>) {
      state.loggingIn = true
    },
    getMeRequest(state, action: PayloadAction<undefined>) {
      state.loggingIn = true
    },
    getMeFailed(state, action: PayloadAction<string>) {
      state.error = action?.payload
      state.loggingIn = false
    },
    getMeSuccess(state, action: PayloadAction<APP.Me>) {
      state.error = undefined
      state.loggingIn = false
      state.me = action.payload
    }
  }
})

export const authActions = authReducer.actions

export const authSelectors = {
  getAccessToken: (state: APP.RootState) => state?.auth?.accessToken,
  isLoggingIn: (state: APP.RootState) => state?.auth?.loggingIn
}
