import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const initialState = {
  appCurrentState: '',
  appStateHistory: []
} as APP.AppState

export const appReducer = createSlice({
  name: 'app',
  initialState,
  reducers: {
    actApp_Init() {},
    actApp_ChangeState(state, action: PayloadAction<string>) {
      state.appCurrentState = action.payload
    }
  }
})

export const appActions = appReducer.actions
