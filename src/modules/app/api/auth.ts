import { API_LOGIN } from './endpoint'
import { unauthenticatedRequest } from '@utils/api.helper'

const createFakePromise: any = (respone: any) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(respone)
    }, 300)
  })
}

export const apiLogin = (_username: string, _password: string) => {
  return createFakePromise({
    email: 'Dortha25@gmail.com',
    first_name: 'Bernardo',
    last_name: 'Hand',
    avatar: 'https://cdn.fakercloud.com/avatars/ariil_128.jpg',
    access_token: '03e596e4-f12a-4bda-b0fc-591085c471ac',
    id: 2
  })
}

export const apiGetMe = () => {
  return createFakePromise({
    email: 'Dortha25@gmail.com',
    first_name: 'Bernardo',
    last_name: 'Hand',
    avatar: 'https://cdn.fakercloud.com/avatars/ariil_128.jpg',
    access_token: '03e596e4-f12a-4bda-b0fc-591085c471ac',
    id: 2
  })
}
