const vi = {
  hello: 'Trang SPLASH',
  onboarding: 'Xin chào các bạn',
  onboarding_login_btn: 'Click để đăng nhập',
  please_update_your_app: 'Hãy cập nhật app!',
  check_for_updates: 'Đang kiểm tra cập nhật...',
  update_needed: 'Cần cập nhật',
  update_we_have_fixed_bug: 'Hãy vui lòng update app',
  update_ok: 'OK',
  update_ignore: 'Bỏ qua',
  login_title: 'Đăng nhập',
  login_submit_btn: 'OK',
  login_user_name: 'Tên đăng nhập',
  login_password: 'Mật khẩu',
  typeAnything: 'Viết đại cái gì đó...',
  auth_enter_user_name_password: 'Hãy nhập mật khẩu và tên đăng nhập'
}

export default vi
