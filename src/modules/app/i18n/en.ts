const en = {
  hello: 'SPLASH PAGE',
  onboarding: 'ONBOARDING PAGE - We can also add a app tour guide here',
  onboarding_login_btn: 'Click to Login',
  please_update_your_app: 'Please update your app!',
  check_for_updates: 'Checking for updates...',
  update_needed: 'Update Needed',
  update_we_have_fixed_bug:
    "We've fixed some bugs to enhance your experience. Kindly update the app to the latest version.",
  update_ok: 'OK',
  update_ignore: 'Ignore',
  login_title: 'Login to the app',
  login_submit_btn: 'Submit',
  login_user_name: 'User name',
  login_password: 'Password',
  typeAnything: 'type anything...',
  auth_enter_user_name_password: 'Please enter your username and password'
}

export default en
