import { AppConfig } from '@m_app/configs/env.config'
import { ReduxStore } from '@m_app/configs/store.config'
import { authSelectors } from '@m_app/reducers/auth.reducer'
import { Alert } from 'react-native'

let accessToken: string = ''

const getAccessToken = () => {
  if (accessToken) return accessToken
  const state = ReduxStore.getState()
  accessToken = authSelectors.getAccessToken(state) || ''
  return accessToken
}

export const buildGetQuery = (params: any) => {
  const query_array = Object.keys(params)
  let query = []
  for (let i = 0; i < query_array?.length; i++) {
    const key = query_array[i]
    if (params[key])
      query.push(
        encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
      )
  }
  return query?.length ? query.join('&') : ''
}

const getUrl = (urlPath: string, external?: boolean) => {
  return external ? urlPath : `${AppConfig.API_URL}${urlPath}`
}

const getHeader = () => {
  const header: any = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'x-access-token': getAccessToken()
  }
  return header
}

export const authenticatedRequest = async (
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH',
  urlPath: string,
  params: object,
  options?: {
    dontUseJsonStringifyParams?: boolean
    isExternalAPI?: boolean
  }
) => {
  const requestInit: RequestInit = {
    method: method,
    headers: await getHeader(),
    // @ts-ignore
    body: !options?.dontUseJsonStringifyParams ? JSON.stringify(params) : params
  }
  const url = getUrl(urlPath, options?.isExternalAPI)
  const res = await fetch(url, requestInit)
  return await handleResponse(res, url, requestInit)
}

export const authenticatedRequestMultipart = async (
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH',
  urlPath: string,
  params: any
) => {
  const options: any = {
    method,
    headers: {
      ...getHeader(),
      'Content-Type': 'multipart/form-data'
    }
  }
  options.body = new FormData()
  for (let name in params) {
    options.body.append(name, params[name])
  }
  const url = getUrl(urlPath)
  const res = await fetch(url, options)
  return await handleResponse(res, url)
}

export const unauthenticatedRequest = async (
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH',
  urlPath: string,
  params?: object,
  options?: {
    dontUseJsonStringifyParams?: boolean
    isExternalAPI?: boolean
  }
) => {
  const requestInit: RequestInit = {
    method,
    headers: {
      ...getHeader(),
      'x-access-token': undefined
    },
    // @ts-ignore
    body: !options?.dontUseJsonStringifyParams ? JSON.stringify(params) : params
  }
  const url = getUrl(urlPath, options?.isExternalAPI)
  const res = await fetch(url, requestInit)
  return await handleResponse(res, url, requestInit)
}

const handleResponse = async (res: any, _url: string, _params?: any) => {
  const resJson = await res.json()
  if (res.status < 400) return resJson
  if (res.status === 401) {
    // TODO: log the user out and request a new session
    Alert.alert('Unauthorized')
  } else {
    const rejectError = new Error()
    // get error message from server
    if (typeof resJson.errors !== 'string') {
      if (resJson.errors instanceof Array) {
        rejectError.message = resJson.errors.join(', ')
      } else {
        rejectError.message = JSON.stringify(res)
      }
    } else {
      rejectError.message = resJson.message
    }
    // TODO: Haven't have time to config Buglogger yet, to config it
    // BugLogger.notify(
    //   new Error(
    //     `API Failed ${url} ${rejectError.message} ${
    //       params ? JSON.stringify(params) : ''
    //     } `
    //   )
    // )
    // @ts-ignore
    throw rejectError
  }
}
