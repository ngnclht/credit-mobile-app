export const appUtil_formatMoney = (
  amount: number,
  hideCent?: boolean,
  ccsymbol?: string,
  skipSpace?: boolean
) => {
  // @ts-ignore
  let formatted = amount.numberFormat(2).toString()
  const space = skipSpace ? '' : ' '
  if (hideCent) formatted = formatted.replace(/\..*/g, '')
  return ccsymbol ? `${ccsymbol}${space}${formatted}` : formatted
}
