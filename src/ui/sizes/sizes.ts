import {
  widthPercentageToDP,
  heightPercentageToDP
} from 'react-native-responsive-screen'

export const wp2dp: (number: number | string) => number = widthPercentageToDP
export const hp2dp: (number: number | string) => number = heightPercentageToDP

export const SIZES = {
  UI_PADDING_MARGIN: wp2dp('5.2%'),

  UI_FONT_SIZE_1: wp2dp('2.9%'), // 9
  UI_FONT_SIZE_2: wp2dp('3.2%'), // 12
  UI_FONT_SIZE_3: wp2dp('3.4%'), // 13
  UI_FONT_SIZE_3a: wp2dp('3.6%'), // 18
  UI_FONT_SIZE_3b: wp2dp('3.85%'), // 18
  UI_FONT_SIZE_4: wp2dp('3.9%'), // 14
  UI_FONT_SIZE_4a: wp2dp('4.1%'), // 19
  UI_FONT_SIZE_5: wp2dp('6.2%'), // 22
  UI_FONT_SIZE_6: wp2dp('6.5%') // 24
}
