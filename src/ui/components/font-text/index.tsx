import React from 'react'
import { Text, StyleSheet } from 'react-native'

// replace system font with your font
const styles = StyleSheet.create({
  regular: {
    fontFamily: 'AvenirNextLTPro-Regular'
  },
  italic: {
    fontFamily: 'system font'
  },
  bold: {
    fontFamily: 'AvenirNextLTPro-Bold'
  }
})

const FONT_WEIGHTS = [
  '100',
  '200',
  '300',
  '400',
  '500',
  '600',
  '700',
  '800',
  '900',
  'normal',
  'bold'
]
const FONTS: any = {
  '100': 'AvenirNextLTPro-Regular',
  '200': 'AvenirNextLTPro-Regular',
  '300': 'AvenirNextLTPro-Regular',
  '400': 'AvenirNextLTPro-Regular',
  '500': 'AvenirNextLTPro-Medium',
  '600': 'AvenirNextLTPro-Medium',
  '700': 'AvenirNextLTPro-Demi',
  '800': 'AvenirNextLTPro-Bold',
  '900': 'AvenirNextLTPro-Heavy',
  normal: 'AvenirNextLTPro-Regular',
  bold: 'AvenirNextLTPro-Bold'
}

export const FontText = (props: { style?: any; children?: any }) => {
  const { style } = props
  let customStyle

  if (style) {
    if (style.fontWeight && FONT_WEIGHTS.includes(style.fontWeight)) {
      let fontFamily = `${FONTS[style.fontWeight]}`
      customStyle = { fontFamily }
    }
    if (style.fontStyle && style.fontStyle === 'italic') {
      customStyle = styles.italic
    }
  }

  return (
    <Text
      {...props}
      allowFontScaling={false}
      style={[styles.regular, style, customStyle]}
    >
      {props.children}
    </Text>
  )
}
