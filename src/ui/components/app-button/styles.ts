import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { hp2dp, SIZES, wp2dp } from "@sizes/sizes";

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: THEMES.MAIN_BUTTON_BACKGROUND,
    borderRadius: wp2dp(8),
    alignItems: 'center',
    justifyContent: 'center',
    height: hp2dp(7)
  },
  disabled: {
    backgroundColor: THEMES.MAIN_BUTTON_DISABLED_BACKGROUND
  },
  title: {
    color: THEMES.TEXT_1,
    fontSize: SIZES.UI_FONT_SIZE_4,
    fontWeight: '700'
  }
})

export default styles
