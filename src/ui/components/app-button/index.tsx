import React from 'react'
import { ActivityIndicator, TouchableOpacity, ViewStyle } from 'react-native'
import { FontText } from '@components/font-text'
import styles from './styles'
import THEMES from '@themes/theme'

type AspireButtonProps = {
  working?: boolean
  disabled?: boolean
  title: string
  onTouch: () => void
  style?: ViewStyle
}

const AspireButton = (props: AspireButtonProps) => {
  return (
    <TouchableOpacity
      disabled={props.disabled || props.working}
      style={[
        styles.wrapper,
        props.style,
        props.disabled ? styles.disabled : undefined
      ]}
      onPress={props.onTouch}
    >
      {props.working ? (
        <ActivityIndicator color={THEMES.TEXT_1} />
      ) : (
        <FontText style={styles.title}>{props.title}</FontText>
      )}
    </TouchableOpacity>
  )
}

export default AspireButton
