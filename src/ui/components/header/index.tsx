import React from 'react'
import { Image, TouchableOpacity, View } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { FontText } from '@components/font-text'
import { appHeaderAssets } from '@components/header/assets'
import styles from './styles'
import { wp2dp } from '@sizes/sizes'
import THEMES from '@themes/theme'
import { appGoBack } from '@m_app/utils/navigation/go-back'

type AppHeaderProps = {
  title?: string
  showBackButton?: boolean
}

export const AppHeader = (props: AppHeaderProps) => {
  const goBack = appGoBack()
  return (
    <View style={styles.wrapper}>
      <View style={styles.container}>
        {props.showBackButton ? (
          <TouchableOpacity style={styles.backButtonContainer} onPress={goBack}>
            <Ionicons
              name={'chevron-back'}
              size={wp2dp(6)}
              color={THEMES.TEXT_1}
            />
          </TouchableOpacity>
        ) : null}
        <FontText style={styles.title}>{props.title}</FontText>
      </View>
      <Image source={appHeaderAssets.logo} style={styles.logo} />
    </View>
  )
}

export default AppHeader
