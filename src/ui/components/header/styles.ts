import THEMES from '@themes/theme'
import { StyleSheet } from 'react-native'
import { SIZES, wp2dp } from '@sizes/sizes'
import { isIOS } from '@m_app/utils/info'

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: THEMES.BACKGROUND_2,
    flexDirection: 'row',
    width: wp2dp(100),
    justifyContent: 'flex-start'
  },
  container: {
    width: wp2dp(100),
    paddingHorizontal: SIZES.UI_PADDING_MARGIN
  },
  title: {
    color: THEMES.TEXT_1,
    fontSize: SIZES.UI_FONT_SIZE_6,
    fontWeight: 'bold',
    marginTop: SIZES.UI_PADDING_MARGIN
  },
  logo: {
    position: 'absolute',
    resizeMode: 'contain',
    right: SIZES.UI_PADDING_MARGIN,
    top: isIOS ? 5 : 0
  },
  backButtonContainer: {
    paddingRight: 20
  }
})

export default styles
