import { AppConfig } from '@m_app/configs/env.config'

class AppGlobalSingleTon {
  private app: string
  private bookDetailFlatListRef: any

  constructor() {
    this.app = AppConfig.APP
    this.bookDetailFlatListRef = undefined
  }

  public isApp1() {
    return this.app == 'appForEndUser'
  }

  public isApp2() {
    return this.app == 'appForAspireBanker'
  }
}

export const AppGlobal = new AppGlobalSingleTon()
export default AppGlobal
