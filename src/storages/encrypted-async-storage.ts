import { AppConfig } from '@m_app/configs/env.config'
import RNAsyncStorage from '@react-native-async-storage/async-storage'
import CryptoJS from 'crypto-js'

export const setItem = async (key: string, text: string) => {
  const encrypted = CryptoJS.AES.encrypt(text, AppConfig.PASSWORD).toString()
  return await RNAsyncStorage.setItem(key, encrypted)
}

export const getItem = async (key: string) => {
  const raw = await RNAsyncStorage.getItem(key)
  if (!!raw) {
    const bytes = CryptoJS.AES.decrypt(raw, AppConfig.PASSWORD)
    const decrypted = bytes.toString(CryptoJS.enc.Utf8)
    return decrypted
  }
  return ''
}

export const removeItem = async (key: string) => {
  return await RNAsyncStorage.removeItem(key)
}

export const AsyncStorage = {
  setItem,
  getItem,
  removeItem
}
