#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>
#import "error.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>

@property (nonatomic, strong) UIWindow *window;

@end
