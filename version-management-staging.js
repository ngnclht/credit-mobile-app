// version that we blocked user from using
const StopVersion = {
  app1: {
    ios: '0.0.0 - 0.0.9',
    android: '0.0.0 - 0.0.9'
  },
  app2: {}
}
// version we are allow user to use
const OKVersion = {
  app1: {
    ios: '1.0.0 - 1.0.1',
    android: '1.0.0 - 1.0.1'
  },
  app2: {}
}

module.exports = {
  StopVersion,
  OKVersion
}
