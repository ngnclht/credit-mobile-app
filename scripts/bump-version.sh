#!/bin/sh
NEXT_VERSION_NAME=1.0.1

echo $SKIP_ASK_VERSION
if [ $SKIP_ASK_VERSION == 1 ]; then
  echo "Skip verification"
  # this is called from fastlane, so we will need to navigate to correct folder
  pwd
else
  echo First, please make sure you set the version name and version code for both android and ios. Press enter to continue
  echo $NEXT_VERSION_NAME
  read ok
fi

NEXT_VERSION_CODE=$(git rev-list --count HEAD)

rm -rf "$(pwd)"/android/app/version.properties
echo "version.name=$NEXT_VERSION_NAME
version.code=$NEXT_VERSION_CODE" >> "$(pwd)"/android/app/version.properties
echo $ANDROID_VERSION_NAME
echo $NEXT_VERSION_CODE
# all ios targets
cd .. 
xcrun agvtool new-version -all $NEXT_VERSION_CODE
agvtool new-marketing-version $NEXT_VERSION_NAME
agvtool next-version -all
cd ..
