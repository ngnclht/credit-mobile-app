set -e

if [ $1 == "debitApp" ]; then
    if [ $2 == "staging" ]; then
        cd android
        ENVFILE=.env.debitApp.staging ./gradlew installDebitAppStagingDebug
        cd ..
        react-native start --reset-cache
    elif [ $2 == "prod" ]; then
        cd android
        ENVFILE=.env.debitApp.prod ./gradlew installDebitAppProdDebug
        cd ..
    fi
fi
